from keyboards import inline_kb1,inline_kb2,list_items, list_btn,inline_kb5,inline_kb6
from database import list_db



async def list_item(bot,callback_query):
    datas=callback_query.data.split('_')
    if datas[2]=='add' or (len(datas)==6 and datas[5]=='add'):
        print(datas)
        await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
        if len(datas)==6:
            await bot.send_message(callback_query.from_user.id, '<b>Do\'koni kiriting.</b>\n<b>Nomi</b>:Mini market\n<b>Telefon raqam</b>:997046599',parse_mode='HTML')
        else:
            await bot.send_message(callback_query.from_user.id, '<b>Maxsulotni kiriting</b>\n<b>Namuna:</b>Nomi_Tan Narxi_Sotuv narxi', reply_markup=inline_kb6)  
    elif datas[2]=='list':
        if len(datas)==4:
            await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
            if datas[0]=='sale':
                await bot.send_message(callback_query.from_user.id, 'Tugmalar ro\'yxati',reply_markup=list_items(list_db('market',['address_id',int(datas[3])]),callback_query.data))
            elif datas[0]=='list':
                await bot.send_message(callback_query.from_user.id, 'Tugmalar ro\'yxati',reply_markup=list_btn(callback_query.data+'_market',[{'label':'list','text':'Ro\'yxat'},{'label':'add','text':'Qo\'shish'}]))
        elif len(datas)==6:
            await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
            await bot.send_message(callback_query.from_user.id, 'Tugmalar ro\'yxati',reply_markup=list_items(list_db(datas[4],['address_id',int(datas[3])]),callback_query.data))
        elif len(datas)==9 and datas[0]=='sale':
            await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
            await bot.send_message(callback_query.from_user.id, 'Maxsulotlar ro\'yxati',reply_markup=list_items(list_db(datas[7],[]),callback_query.data))
        elif datas[1]=='product':
            txt = '<b>Maxsulotlar ro\'yxati</b>\n\n'
            for item in list_db(datas[1],[]):
                txt+=f'Nomi:{item[1]}\nTan narxi:{item[2]}\nSotuv narxi:{item[3]}\n\n'
            await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
            await bot.send_message(callback_query.from_user.id, txt,reply_markup=inline_kb5)

        elif datas[1]=='address':
            if datas[0]=='sale':
                await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
                await bot.send_message(callback_query.from_user.id, 'Hudud ro\'yxati',reply_markup=list_items(list_db(datas[1],[]),callback_query.data))
            elif datas[0]=='list':
                await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
                await bot.send_message(callback_query.from_user.id, 'Hudud ro\'yxati',reply_markup=list_items(list_db(datas[1],[]),callback_query.data))

async def add_item(bot,callback_query,data):
    print(data)
    if data=='product':
        await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
        await bot.send_message(callback_query.from_user.id, '<b>Maxsulotni soni kiriting</b>\n<b>Namuna:</b>Soni_olgan Suma')
    elif data=='address' :
        await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
        await bot.send_message(callback_query.from_user.id, 'Hududni kiriting')
    elif data=='market':
        await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
        await bot.send_message(callback_query.from_user.id, 'Do\'koni kiriting')      