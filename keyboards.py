from aiogram.types import InlineKeyboardButton,InlineKeyboardMarkup

inline_btn1 = InlineKeyboardMarkup()
inline_btn2 = InlineKeyboardMarkup()
inline_btn3 = InlineKeyboardMarkup()
inline_btn4 = InlineKeyboardMarkup()
inline_btn5 = InlineKeyboardMarkup()
inline_btn6 = InlineKeyboardMarkup()
#first step three buttons
list_btn = InlineKeyboardButton('Ro\'yxat', callback_data='list')
sale_btn = InlineKeyboardButton('Sotish', callback_data='sale_address_list')
anality_btn = InlineKeyboardButton('anality', callback_data='anality')
home = InlineKeyboardButton('🏠 Bosh sahifa',callback_data='home')

#second step into three buttons list
address = InlineKeyboardButton('Hudud', callback_data='list_address')
product = InlineKeyboardButton('Mahsulot', callback_data='list_product')
back_home = InlineKeyboardButton('⬅️ Orqaga', callback_data='home')

#third step into three buttons list
address_list = InlineKeyboardButton('Ro\'yxatlar', callback_data='list_address_list')
address_add = InlineKeyboardButton('Qo\'shish', callback_data='list_address_add')
back_list = InlineKeyboardButton('⬅️ Orqaga', callback_data='list')

#third step into three buttons list
product_list = InlineKeyboardButton('Ro\'yxatlar', callback_data='list_product_list')
product_add = InlineKeyboardButton('Qo\'shish', callback_data='list_product_add')
back_product = InlineKeyboardButton('⬅️ Orqaga', callback_data='list_product')
add_address = InlineKeyboardButton('Hudud', callback_data='add_address')
add_market = InlineKeyboardButton('Do\'kon', callback_data='add_market')
add_product = InlineKeyboardButton('Mahsulot', callback_data='add_product')

def list_address(data):
    inline_btn = InlineKeyboardMarkup()
    inline_btn.add(InlineKeyboardButton('Mahsulot', callback_data='add_product'))
    for item in data:
        print(item[0])
    return inline_btn    
    # for item in data:
    #     print(item[0])
    #     await inline_btn.row(InlineKeyboardButton(list(item)[1], callback_data=f'add_address_list_{list(item)[0]}'))
    # return inline_btn

inline_kb1 = inline_btn1.row(sale_btn,list_btn,anality_btn)
inline_kb2 = inline_btn2.row(address,product)
inline_kb2 = inline_btn2.row(back_home,home)
inline_kb3 = inline_btn3.row(address_list,address_add)
inline_kb3 = inline_btn3.row(back_list,home)
inline_kb4 = inline_btn4.row(product_list,product_add)
inline_kb4 = inline_btn4.row(back_list,home)
inline_kb5 = inline_btn5.row(back_product,home)
inline_kb6 = inline_btn6.row(back_product,home)


backs=''

def list_items(data,datas):
    inline_btn = InlineKeyboardMarkup()
    global backs
    if datas.split('_')[0]!='sale':
        if len(datas.split('_'))==6:
            backs = '_'.join(datas.split('_')[:-2])
        else:
            backs = '_'.join(datas.split('_')[:-1])    
    else:
        if 'sale_address_list'==datas:
            backs = 'home'
        else:
            backs = '_'.join(datas.split('_')[:-3])
        # if datas.split('_')
        # backs = '_'.join(datas.split('_')[:-1])
        
    if datas[:4]=='sale':
        for item in data:
            if len(datas.split('_'))==3:
                inline_btn.add(InlineKeyboardButton(item[1], callback_data=f'{datas}_{item[0]}_market_list'))
            elif len(datas.split('_'))==6:
                inline_btn.add(InlineKeyboardButton(item[1], callback_data=f'{datas}_{item[0]}_product_list'))
            elif len(datas.split('_'))==9:
                inline_btn.add(InlineKeyboardButton(item[1], callback_data=f'{datas}_{item[0]}_order_add'))
            else:
                inline_btn.add(InlineKeyboardButton(item[1], callback_data=f'{datas}_{item[0]}'))
    for item in data:
        inline_btn.add(InlineKeyboardButton(item[1], callback_data=f'{datas}_{item[0]}'))
    inline_btn.add(InlineKeyboardButton('⬅️ Orqaga', callback_data=f'{backs}'))    
    return inline_btn


def list_btn(data,datas):
    inline_btn = InlineKeyboardMarkup()
    # print(data,datas)
    for item in datas:
        inline_btn.add(InlineKeyboardButton(item['text'], callback_data=data+'_'+item['label']))
    return inline_btn
"""
list_address
list_address_list
list_address_add
list_address_list_id_market_list
list_address_list_id_market_add
list_product
list_product_list
list_product_add
"""

"""
sale_address_list
sale_address_list_id_market_list
sale_address_list_id_market_list_id_product_list
sale_address_list_id_market_list_id_product_list_id_order_add
"""
