import sqlite3

conn = sqlite3.connect('SQLite.db')

cur = conn.cursor()
# print(cur.fetchone())

def insert_to_db(table_name,column_names,table_values):
    cur.execute(f"INSERT INTO {table_name} {column_names} VALUES {table_values}")
    conn.commit()

def list_db(table_name,whry):
    if len(whry)!=0:    
        return cur.execute(f"Select * from {table_name} Where {whry[0]}={whry[1]}")
    return cur.execute(f"Select * from {table_name}")    

# conn.close()