from main import dp, bot

from aiogram.types import Message,CallbackQuery
from config import admin_id
from keyboards import inline_kb1,inline_kb2, inline_kb3,inline_kb4,list_items
from services import add_item,list_item
from database import insert_to_db,list_db

add_info = ""
id_info = {}
add_query=[]


@dp.callback_query_handler()
async def process_callback_button1(callback_query: CallbackQuery):
    global add_info,add_query
    data=callback_query.data.split('_')
    if 'home' in data:
        await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
        await bot.send_message(callback_query.from_user.id, 'Tugmalarni tanlang',reply_markup=inline_kb1)
    elif len(data)==1:
        await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
        await bot.send_message(callback_query.from_user.id, 'Tugmalarni tanlang',reply_markup=inline_kb2)
    elif len(data)==2:
        if data[1]=='address':
            await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
            await bot.send_message(callback_query.from_user.id, 'Tugmalarni tanlang',reply_markup=inline_kb3)
        elif data[1]=='product':
            await bot.delete_message(callback_query.message.chat.id,callback_query.message.message_id)
            await bot.send_message(callback_query.from_user.id, 'Tugmalarni tanlang',reply_markup=inline_kb4)
    elif len(data)==3:
        if data[0]=='sale':
            await list_item(bot,callback_query)
        elif data[0]=='list':
            add_info=data[1]
            await list_item(bot,callback_query)
    elif len(data)==4 or len(data)==6:
        if len(data)==6:
            add_info='market'
            id_info['address_id']=data[3]
        await list_item(bot,callback_query)
    elif len(data)==7:
        await list_item(bot,callback_query)
    elif len(data)==9:
        await list_item(bot,callback_query)
    else:
        add_info = data[1]
        add_query = callback_query.data.split('_')
        print(add_info)
        if data[0]=='list':
            if len(data)==6:
                await add_item(bot,callback_query,'market')
            else:
                await add_item(bot,callback_query,add_info)    
        elif data[0]=='sale':  
            add_info = data[10]
            await add_item(bot,callback_query,add_info)
            
@dp.message_handler(commands=['start'])
async def process_start_command(message: Message):
    await bot.send_message(message.from_user.id,"Tizimga xush kelibsiz. Tugmalardan birini bosing",reply_markup=inline_kb1)


@dp.message_handler()
async def echo_message(msg: Message):
    await bot.delete_message(msg.chat.id,msg.message_id)
    await bot.delete_message(msg.chat.id,msg.message_id-1)
    if add_info=='address':
        insert_to_db('address',('name','sub'),(msg.text,0))
    elif add_info=='market':
        infos = str(msg.text).split('_')
        insert_to_db('market',('name','phone','address_id'),(infos[0],infos[1],id_info['address_id']))
    elif add_info=='product':
        infos = str(msg.text).split('_')
        insert_to_db('product',('name','price','sale_price'),(infos[0],infos[1],infos[2]))
    elif add_info=='order':
        infos = str(msg.text).split('_')
        insert_to_db('orders',('count_product','paid','market_id','product_id'),(infos[0],infos[1],add_query[6],add_query[9]))
        await bot.send_message(msg.from_user.id, 'Nimani qo\'shmoqchisiz',reply_markup=list_items(list_db(add_query[7],[]),'_'.join(add_query[:-5])))        